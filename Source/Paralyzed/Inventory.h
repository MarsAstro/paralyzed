// Copyright OSS 2017

#pragma once

#include "GrabbingInterface.h"
#include "GrabbableObject.h"
#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/Actor.h"
#include "Inventory.generated.h"

class UBoxComponent;
class AGrabMotionController;

UCLASS()
class PARALYZED_API AInventory : public AGrabbableObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInventory();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Initializes models
	UFUNCTION(BlueprintCallable, Category = "Setup")
		void Initialize(UStaticMeshComponent* BagMesh, UBoxComponent* Collision, AGrabMotionController* LeftHand, AGrabMotionController* RightHand);

	UFUNCTION(BlueprintCallable, Category = "Widgets")
		AGrabbableObject* GetItemInSlot(int RequestedSlot);

	// Called by wheelchair so that the inventory can confirm that it has a connection
	void ConnectToWheelChair();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:	
	// A place for items in inventory to gather
	FVector TheVoid = FVector(0.0F, 0.0F, -10000);
	// Physical representation in game
	UStaticMeshComponent* BasketMesh;
	// To detect overlap events
	UBoxComponent* Collision;
	// Need references to both hands in order to detect if an object is grabbed
	AGrabMotionController* LeftHand;
	AGrabMotionController* RightHand;
	// Contains all items
	TArray<AGrabbableObject*> Inventory;
	// Is it connected to wheelchair? if not, it will be grabbable
	bool bConnectedToWheelChair = false;

	// This is used to determine actions for different overlap events such as an item being put in
	// or the hands overlapping, wishing to extract items-------------------hmm-------------------
	void OverlapEvents();
	// If the overlapping actor is an item, put it in the inventory array
	void PutInBag(AActor* Item);
	// Extracting item from bag
	void ExtractFromBag(UMotionControllerComponent* HandRequestingItem);

	// Abstract classes from IGrabbingInterface
	// Stores the logic for what is happening with controller inputs
	void Grabbed_Implementation(UMotionControllerComponent* GrabbingController) override;
	void Released_Implementation(UMotionControllerComponent* GrabbingController) override;
};
