// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Checkpoint.generated.h"

class ARacemanager;

/**
 * 
 */
UCLASS( ClassGroup = (Custom), meta = (BlueprintSpawnableComponent) )
class PARALYZED_API UCheckpoint : public UBoxComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category="Setup")
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult &SweepResult);
	
	UFUNCTION(BlueprintCallable, Category="Setup")
	void SetGoal();

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetNextCheckpoint(UCheckpoint* Checkpoint);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	UCheckpoint* GetNextCheckpoint();

	UFUNCTION(BlueprintCallable, Category = "Setup")
	bool IsGoal() { return bIsGoal; };

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetNumberOfLaps(int NumberOfLaps);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetRaceManager(ARaceManager* RaceManager);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int ID;

protected:
	void BeginPlay() override;

private:
	ARaceManager* RaceManager = nullptr;

	UCheckpoint* NextCheckpoint = nullptr;

	UPROPERTY(VisibleAnywhere)
	bool bIsGoal = false;

	int NumberOfLaps = 0;
};
