// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ParalyzedGameInstance.generated.h"

class UParalyzedSaveGame;
class UInputComponent;
class UGameplayStatics;
class UWorld;

/**
 * 
 */
UCLASS()
class PARALYZED_API UParalyzedGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
	
public:
	void Load(FString Filename, int32 UserIndex = 0);
	void Save(FString Filename, int32 UserIndex = 0);

	// Is currently called from gamemode since GameInstance doesn't contain a begin play
	void SetupInput();

private:
	virtual void Init() override;

	// Debug functions
	void QuickSave();
	void QuickLoad();

	// Instance containing all savedata
	UParalyzedSaveGame* SaveGameInstance;
	
	// Debug
	UPROPERTY(EditAnywhere)
	int TestNumber = 1;
};
