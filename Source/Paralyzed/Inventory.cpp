// Copyright OSS 2017

#include "Inventory.h"
#include "GrabMotionController.h"
#include "MotionControllerComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"


// Sets default values
AInventory::AInventory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

/**
* Will get references from the blueprint class and set these accessible in the c++ code
* @param BasketMesh - The mesh for the basket itself
* @param Collision - A box collision to detect if actors are overlapping
* @param LeftHand - A pointer to the left hand
* @param RightHand - A pointer to the right hand
*/
void AInventory::Initialize(UStaticMeshComponent* BasketMesh, UBoxComponent* Collision, AGrabMotionController* Hand1, AGrabMotionController* Hand2)
{
	this->BasketMesh = BasketMesh;
	this->Collision = Collision;

	// Checks if hands actually exist before figuring out what hand it is, in case unreal is
	// simulating instead of playing in vr mode
	if (Hand1 && Hand1->GetMotionControllerComponent()->Hand == EControllerHand::Left)
	{
		LeftHand = Hand1;
		RightHand = Hand2;
	}
	else if (Hand1)
	{
		LeftHand = Hand2;
		RightHand = Hand1;
	}

	GetStaticMeshComponent()->SetSimulatePhysics(true);
	GetStaticMeshComponent()->SetCollisionProfileName("BlockAllDynamic");
	GetStaticMeshComponent()->bGenerateOverlapEvents = true;
	GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
	GetStaticMeshComponent()->WakeRigidBody();
}

AGrabbableObject* AInventory::GetItemInSlot(int RequestedSlot)
{
	if (Inventory.Num() > 0)
	{
		return Inventory[RequestedSlot % Inventory.Num()];
	}

	return nullptr;
}

void AInventory::ConnectToWheelChair()
{
	bConnectedToWheelChair = true;
}

// Called when the game starts or when spawned
void AInventory::BeginPlay()
{
	Super::BeginPlay();
}

/**
* Checks for overlapping actors and puts them in an array. This array will then be checked for actors
* that are not hands for inserting items into the Inventory array, or if it is hands, it will start
* the extract items function
*/
void AInventory::OverlapEvents()
{
	// Array of actors that are overlapping, gets casted to AGrabbableObject in the PutInBag() function,
	// but as for now, unreal wants an array of actors to check
	TArray<AActor*> Colliding;
	Collision->GetOverlappingActors(Colliding, TSubclassOf<AGrabbableObject>());

	for (AActor* CollidingActor : Colliding)
	{
		// Checks if hands exist. Necessary to use regular simulation without VR
		if (LeftHand != nullptr && RightHand != nullptr)
		{
			// This check ensures that the item is not grabbed by any of the hads by comparing it to the hand's
			// grabbed object reference
			if (CollidingActor != LeftHand->GetGrabbedObjectReference() && CollidingActor != RightHand->GetGrabbedObjectReference() &&
				CollidingActor != LeftHand								&& CollidingActor != RightHand)
			{
				PutInBag(CollidingActor);
			}
		}
		else
		{
			PutInBag(CollidingActor);
		}
	}
}

/**
* Sets the item far under the world and puts the reference in the Inventory array
* @param Item - item reference as an actor
*/
void AInventory::PutInBag(AActor* Item)
{
	AGrabbableObject* Object = Cast<AGrabbableObject>(Item);

	// If the object was not a grabbable object to begin with, it will be null.
	// Mostly used to prevent the hands from being detected
	if (Object == nullptr)
	{
		return;
	}

	Inventory.Add(Object);
	Object->GetStaticMeshComponent()->SetSimulatePhysics(false);
	Object->GetStaticMeshComponent()->SetCollisionProfileName("OverlapAllDynamic");
	Object->SetActorLocation(TheVoid);
}

/**
* Extracts item from basket
* @param HandRequestingItem - a reference to the hand attempting to extract an item
*/
void AInventory::ExtractFromBag(UMotionControllerComponent* HandRequestingItem)
{
	if (Inventory.Num() > 0)
	{
		Inventory[0]->SetActorLocation(HandRequestingItem->GetComponentLocation());
		Inventory[0]->Execute_Grabbed(Inventory[0], HandRequestingItem);

		if (HandRequestingItem->Hand == EControllerHand::Left)
		{
			LeftHand->SetGrabbedObjectReference(Inventory[0]);
		}
		else
		{
			RightHand->SetGrabbedObjectReference(Inventory[0]);
		}

		Inventory.RemoveAt(0);
	}
}

// Called every frame
void AInventory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bConnectedToWheelChair)
	{
		OverlapEvents();
	}
}

void AInventory::Grabbed_Implementation(UMotionControllerComponent* GrabbingController)
{

	if (bConnectedToWheelChair)
	{
		UE_LOG(LogTemp, Warning, TEXT("It IS connected... %d"), bConnectedToWheelChair);
		ExtractFromBag(GrabbingController);
	}
	else
	{
		Super::Grabbed_Implementation(GrabbingController);
	}
}

void AInventory::Released_Implementation(UMotionControllerComponent* GrabbingController)
{
	if (!bConnectedToWheelChair)
	{
		Super::Released_Implementation(GrabbingController);
	}
}