// Fill out your copyright notice in the Description page of Project Settings.

#include "ParalyzedGameModeBase.h"

#include "ParalyzedGameInstance.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"

void AParalyzedGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// Artificial begin play for game instance
	((UParalyzedGameInstance*)GetGameInstance())->SetupInput();
}
