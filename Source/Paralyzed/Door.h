// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/Actor.h"
#include "Door.generated.h"

class UShapeComponent;
class USphereComponent;
class AGrabbableObject;

UCLASS()
class PARALYZED_API ADoor : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Checks if the key is inside the door's trigger-volume
	UFUNCTION(BlueprintCallable, Category = "Door")
		bool KeyInside();
	UFUNCTION(BlueprintCallable, Category = "Setup")
		void Initialize(UStaticMeshComponent* Body, UStaticMeshComponent* Handle,
						UStaticMeshComponent* Keyhole, USphereComponent* KeyholeCollision);


	UPROPERTY(EditAnywhere)
		FName KeyTag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bOpen = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float rotation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	// Body mesh
	UStaticMeshComponent* Body;
	// Handle mesh
	UStaticMeshComponent* Handle;
	// KeyHole mesh
	UStaticMeshComponent* KeyHole;
	// keyhole trigger-volume
	USphereComponent* KeyholeCollision;
	//tag


};
