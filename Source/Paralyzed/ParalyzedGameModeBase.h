// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ParalyzedGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PARALYZED_API AParalyzedGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

private:
	void BeginPlay() override;

};
