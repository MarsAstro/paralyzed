// Copyright OSS 2017

#include "ParalyzedGameInstance.h"

#include "ParalyzedSaveGame.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

/*
* Saves all current content of the SaveGameInstance
* @param Filename - Name of the file that the data will be saved to
* @param UserIndex - UserIndex used when saving file, default = 0
*/
void UParalyzedGameInstance::Save(FString Filename, int32 UserIndex)
{
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, Filename, UserIndex);
}

/*
* Loads content from specified file
* @param Filename - Name of the file that the data will be saved to
* @param UserIndex - UserIndex used when loading file, default = 0
*/
void UParalyzedGameInstance::Load(FString Filename, int32 UserIndex)
{
	SaveGameInstance = Cast<UParalyzedSaveGame>(UGameplayStatics::LoadGameFromSlot(Filename, UserIndex));
}

void UParalyzedGameInstance::Init()
{
	Super::Init();

	SaveGameInstance = Cast<UParalyzedSaveGame>(UGameplayStatics::CreateSaveGameObject(UParalyzedSaveGame::StaticClass()));
}

/*
* For use in debugging. Saves to quicksave slot
*/
void UParalyzedGameInstance::QuickSave()
{
	SaveGameInstance->TestNumber = TestNumber;
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("QuickSave"), 0);
}

/*
* For use in debugging. Loads from quicksave slot
*/
void UParalyzedGameInstance::QuickLoad()
{
	SaveGameInstance = Cast<UParalyzedSaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("QuickSave"), 0));
	TestNumber = SaveGameInstance->TestNumber;
}

/*
* Binds keys for quicksave and quickload
*/
void UParalyzedGameInstance::SetupInput()
{
	UWorld* World = GetWorld();
	if (World)
	{
		World->GetFirstPlayerController()->InputComponent->BindAction("QuickSave", IE_Pressed, this, &UParalyzedGameInstance::QuickSave);
		World->GetFirstPlayerController()->InputComponent->BindAction("QuickLoad", IE_Pressed, this, &UParalyzedGameInstance::QuickLoad);
	}
}


