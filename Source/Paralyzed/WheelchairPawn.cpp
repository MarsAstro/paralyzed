// Copyright OSS 2017

#include "WheelchairPawn.h"
#include "Checkpoint.h"
#include "Components/BoxComponent.h"

AWheelchairPawn::AWheelchairPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	CurrentLap = 1;
	DistanceToNextSection = 0.0f;
}


void AWheelchairPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (LastVisitedCheckpoint != nullptr)
	{
		SetDistanceToNextSection(LastVisitedCheckpoint->GetNextCheckpoint()->GetComponentTransform().GetLocation());
	}
}

void AWheelchairPawn::InitializeGrabBoxes(UBoxComponent * RightGrabBox, UBoxComponent * LeftGrabBox)
{
	this->RightGrabBox = RightGrabBox;
	this->LeftGrabBox = LeftGrabBox;
}

void AWheelchairPawn::SetDistanceToNextSection(FVector SectionLocation)
{
	FVector Distance = (GetActorLocation() - SectionLocation);
	DistanceToNextSection = Distance.Size();
}

void AWheelchairPawn::PlaceAt(FVector Position, FQuat Rotation)
{
	TInlineComponentArray<UPrimitiveComponent*> Components;
	GetComponents(Components);
	
	FVector RightGrabBoxLocation = RightGrabBox->GetRelativeTransform().GetLocation();
	FVector LeftGrabBoxLocation = LeftGrabBox->GetRelativeTransform().GetLocation();

	for (UPrimitiveComponent* Component : Components)
	{
		// The checkpoint location + offset in Z (height)
		Component->SetAllPhysicsPosition(Position);
		// Calculating the correct forward rotation for the racer
		Component->SetAllPhysicsRotation(Rotation.Rotator());
	}

	// These has to be moved separately from the other components
	RightGrabBox->SetRelativeLocation(RightGrabBoxLocation);
	LeftGrabBox->SetRelativeLocation(LeftGrabBoxLocation);
}

FString AWheelchairPawn::GetName()
{
	return Name;
}

void AWheelchairPawn::SetName(FString Name)
{
	this->Name = Name;
}
