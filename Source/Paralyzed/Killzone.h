// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Killzone.generated.h"

class UBoxComponent;
class AWheelchair;

UCLASS()
class PARALYZED_API AKillzone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKillzone();

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialize(UBoxComponent* TriggerVolume);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
						AActor* OtherActor,
						UPrimitiveComponent* OtherComp,
						int32 OtherBodyIndex,
						bool bFromSweep,
						const FHitResult &SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UBoxComponent* TriggerVolume = nullptr;
};
