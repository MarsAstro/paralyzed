// Fill out your copyright notice in the Description page of Project Settings.

#include "Door.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Core/Public/Math/TransformNonVectorized.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
	
	AActor* Owner = GetOwner();
	
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Returns true if a key is inside the collision sphere of the door
bool ADoor::KeyInside()
{
	TArray<AActor*> colliding;
	KeyholeCollision->GetOverlappingActors(colliding);

	// This will cycle through actors inside the volume and check if it's the right key
	for (AActor* CollidingActor : colliding)
	{
	// If the actor colliding has the right keyTag
		if (CollidingActor->ActorHasTag(KeyTag))
		{
			return true;
		}
	}

	return false;
}

// Initialize door variables so that they can be used in both blueprints and c++
void ADoor::Initialize(UStaticMeshComponent* Body, UStaticMeshComponent* Handle, UStaticMeshComponent* Keyhole, USphereComponent* KeyholeCollision)
{
	this->Body = Body;
	this->Handle = Handle;
	this->KeyHole = Keyhole;
	this->KeyholeCollision = KeyholeCollision;

	rotation = Body->RelativeRotation.Yaw;
}


