// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MyUserWidget.generated.h"

/**
 * Used to create components during runtime and perform actions closely
 * correlated with them, like making textures
 */
UCLASS()
class PARALYZED_API UMyUserWidget : public UUserWidget
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = ComponentHandling)
		static UButton* CreateButton(const FString& ButtonName);

	UFUNCTION(BlueprintCallable, Category = ComponentHandling)
		static UCanvasPanel* CreateCanvasPanel();
	
	UFUNCTION(BlueprintCallable, Category = ComponentHandling)
		static UImage* CreateImage(const FString& Path, const FString& TextureName);

	UFUNCTION(BlueprintCallable, Category = ComponentHandling)
		static UScaleBox* CreateScaleBox();

	UFUNCTION(BlueprintCallable, Category = ComponentHandling)
		static UTextBlock* CreateTextBlock(const FString& Text);

	UFUNCTION(BlueprintCallable, Category = ImageHandling)
		static UTexture2D* GetTextureByName(const FString& Path, const FString& TextureName);
};
