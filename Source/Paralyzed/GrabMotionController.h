// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GrabMotionController.generated.h"

class USphereComponent;
class USkeletalMeshComponent;
class UMotionControllerComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHapticDelegate, float, Intensity);

UCLASS()
class PARALYZED_API AGrabMotionController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrabMotionController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void RelinquishObjectOwnership();
	void RumbleController(float Intensity);
	void ResetRumble();

	// Used to compare items to figure out if the hand is currently holding it
	AActor* GetGrabbedObjectReference();
	// Used to set grabbed object externally for the purpose of extracting from inventory for now
	void SetGrabbedObjectReference(AActor* Item);

	// Get the motion controller from this controller (used in inventory's initialize to dicern hands)
	UMotionControllerComponent* GetMotionControllerComponent();

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialize(UMotionControllerComponent* MotionController, USkeletalMeshComponent* HandMesh, USphereComponent* GrabSphere);

	UPROPERTY(BlueprintAssignable)
	FHapticDelegate Rumble;

private:
	UFUNCTION(BlueprintCallable, Category = "Grabbing")
	void GrabFirstGrabbableObject();

	UFUNCTION(BlueprintCallable, Category = "Grabbing")
	void ReleaseGrabbedObject();

	UMotionControllerComponent* MotionController;
	USkeletalMeshComponent* HandMesh;
	USphereComponent* GrabSphere;
	AActor* GrabbedObject = nullptr;

	bool HasRumbled = false;
};
