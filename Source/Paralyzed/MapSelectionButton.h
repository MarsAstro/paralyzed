// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "MapSelectionButton.generated.h"

/**
 * 
 */
UCLASS()
class PARALYZED_API UMapSelectionButton : public UButton
{
	GENERATED_BODY()

	FString* MapSelectionWidgetReference;

	virtual FOnButtonClickedEvent OnClicked();
};
