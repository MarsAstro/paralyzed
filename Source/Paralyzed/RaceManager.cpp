// Copyright OSS 2017

#include "RaceManager.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "Engine/World.h"
#include "Checkpoint.h"
#include "WheelchairPawn.h"

// Sets default values
ARaceManager::ARaceManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MaxPlayers = 6;
	NumberOfLaps = 3;
	RaceTimer = 0.0f;
	bRaceStarted = true;
}

// Called when the game starts or when spawned
void ARaceManager::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARaceManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RaceTimer += DeltaTime;

	UpdatePlacements();
	if (RaceTimer > 10)
	{
		UE_LOG(LogTemp, Warning, TEXT("Placements:------------------------------------------"));
		for (AWheelchairPawn* Racer : Racers)
		{
			UE_LOG(LogTemp, Warning, TEXT("%s"), *Racer->GetName());
			UE_LOG(LogTemp, Warning, TEXT("%f"), Racer->DistanceToNextSection);
		}
		RaceTimer = 0;
	}
}

void ARaceManager::UpdatePlacements()
{
	// Sorts the Racers to their place in race
	Racers.Sort([](const AWheelchairPawn& Racer1, const AWheelchairPawn& Racer2)
	{
		return  (Racer1.CurrentLap > Racer2.CurrentLap)
			 || (Racer1.CurrentLap == Racer2.CurrentLap && 
				 Racer1.LastVisitedCheckpoint->ID > Racer2.LastVisitedCheckpoint->ID)
			 || (Racer1.CurrentLap == Racer2.CurrentLap && 
				 Racer1.LastVisitedCheckpoint->ID == Racer2.LastVisitedCheckpoint->ID &&
				 Racer1.DistanceToNextSection < Racer2.DistanceToNextSection);
	});
}

void ARaceManager::RacerFinished(AWheelchairPawn* Racer)
{
	Racers.Remove(Racer);
	FinalPlacements.Add(Racer);

		
}

void ARaceManager::SetupRacers(UCheckpoint* Start, TArray<FVector> StartPositions)
{
	int i = 0;
	for (TActorIterator<AWheelchairPawn> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		FString Name = "Racer" + FString::FromInt(i+1);
		AWheelchairPawn* Racer = *ActorItr;
		Racer->SetName(Name);
		Racer->PlaceAt(StartPositions[i] + FVector(0.f, 0.f, 300.f),
			(FVector::CrossProduct(Start->GetRightVector(), -Start->GetUpVector())).ToOrientationQuat());
		Racers.Add(Racer);
		i++;
	}

	// The racers should come from the lobby.
	// Should fill rest of the spots with AIPawns.
	for (; i < MaxPlayers; i++)
	{
		
	}

	UE_LOG(LogTemp, Warning, TEXT("Number of racers: %d"), Racers.Num())
	for (AWheelchairPawn* Racer : Racers)
	{
		Racer->LastVisitedCheckpoint = Start;
	}
}
