// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MapSelection.generated.h"

UCLASS()
class PARALYZED_API AMapSelection : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapSelection();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		bool SetUp(TArray<FString>& MapsFound, TArray<FString>& ImagesFound);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// The full path to where the map files and pictures are located

	FString FullPath;
	
};
