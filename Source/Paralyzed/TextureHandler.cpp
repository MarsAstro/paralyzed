// Copyright OSS 2017

#include "TextureHandler.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"

UTexture2D* UTextureHandler::GetTextureByName(const FString& Path, const FString& TextureName)
{

	FString AssetPath = "Texture2D'/{Path}/{TextureName}.{TextureName}'";
	AssetPath = AssetPath.Replace(TEXT("{TextureName}"), *TextureName).Replace(TEXT("{Path}"), *Path);
	FStringAssetReference AssetRef(AssetPath);

	return Cast<UTexture2D>(AssetRef.TryLoad());
}