// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ParalyzedSaveGame.generated.h"

class APlayerState;

/** 
 * Class that contains all variables that will be saved.
 * For a variable to be able to be saved and loaded it needs to have UPROPERTY(SaveGame)
 */
UCLASS()
class PARALYZED_API UParalyzedSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	// Name of the save slot
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString SaveSlotName = TEXT("test");

	// Index of the user tied to a save slot
	UPROPERTY(VisibleAnywhere, Category = Basic)
	uint32 UserIndex = 0;
	
	UPROPERTY(SaveGame)
	int TestNumber = 0;


};
