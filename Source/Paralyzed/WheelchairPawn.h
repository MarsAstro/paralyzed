// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "WheelchairPawn.generated.h"

class UCheckpoint;
class UBoxComponent;

/**
 * 
 */
UCLASS()
class PARALYZED_API AWheelchairPawn : public AWheeledVehicle
{
	GENERATED_BODY()
	
public:
	// Sets default values for this pawn's properties
	AWheelchairPawn();
	
	// Called every frame
	virtual void Tick(float DeltaTime);

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent);

	UFUNCTION(BlueprintCallable, category = "setup")
	void InitializeGrabBoxes(UBoxComponent* RightGrabBox, UBoxComponent* LeftGrabBox);

	void SetDistanceToNextSection(FVector SectionLocation);

	UPROPERTY(BlueprintReadWrite)
	UCheckpoint* LastVisitedCheckpoint = nullptr;

	void PlaceAt(FVector Position, FQuat Rotation);

	int CurrentLap;
	float DistanceToNextSection;

	FString GetName();
	void SetName(FString Name);

protected:
	// Player info
	FString Name;

	UBoxComponent* RightGrabBox;
	UBoxComponent* LeftGrabBox;
};
