// Copyright OSS 2017

#include "Checkpoint.h"

#include "RaceManager.h"
#include "WheelchairPawn.h"

void UCheckpoint::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult)
{
	AWheelchairPawn* Racer = Cast<AWheelchairPawn>(OtherActor);

	if (Racer == nullptr)
	{
		return;
	}

	if (Racer->LastVisitedCheckpoint == nullptr)
	{
		Racer->LastVisitedCheckpoint = this;
	}

	// This doesnt work cause next = nullptr
	if (Racer->LastVisitedCheckpoint->NextCheckpoint == this)
	{
		Racer->LastVisitedCheckpoint = this;

		if (bIsGoal)
		{
			if (!RaceManager)
			{
				UE_LOG(LogTemp, Error, TEXT("Goal could not find racemanager"))
			}
			else if (Racer->CurrentLap < RaceManager->GetNumberOfLaps())
			{
				Racer->CurrentLap++;
			}
			else
			{
				RaceManager->RacerFinished(Racer);
				// TODO: Place Racer as a spectator
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("Lap: %d, Checkpoint: %d"), Racer->CurrentLap, ID);
	}
}

void UCheckpoint::SetGoal()
{
	bIsGoal = true;
}

void UCheckpoint::SetNextCheckpoint(UCheckpoint* Checkpoint)
{
	this->NextCheckpoint = Checkpoint;
}

UCheckpoint* UCheckpoint::GetNextCheckpoint()
{
	return NextCheckpoint;
}

void UCheckpoint::SetNumberOfLaps(int NumberOfLaps)
{
	this->NumberOfLaps = NumberOfLaps;
}

void UCheckpoint::SetRaceManager(ARaceManager * RaceManager)
{
	this->RaceManager = RaceManager;
}

void UCheckpoint::BeginPlay()
{
	this->OnComponentBeginOverlap.AddDynamic(this, &UCheckpoint::OnOverlapBegin);
}
