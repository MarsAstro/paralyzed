// Copyright OSS 2017

#include "MapSelection.h"
#include "Runtime/Core/Public/HAL/FileManager.h"
#include "Runtime/Core/Public/Misc/Paths.h"


// Sets default values
AMapSelection::AMapSelection()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
}

bool AMapSelection::SetUp(TArray<FString>& MapsFound, TArray<FString>& ImagesFound)
{
	// Sets up the path to the game, followed by the fixed path
	FullPath = FPaths::GameDir();
	FullPath.Append("/Content/Maps/RaceMaps");

	//TArray<FString> MapsFound;
	//TArray<FString> ImagesFound;

	// Need a reference to the filemanager that will find all the files in the directory
	// Maps are umaps and textures are uassets. Nothing else should be placed in the RaceMaps folder
	IFileManager& FileManager = IFileManager::Get();
	FileManager.FindFiles(MapsFound, *FullPath, TEXT("*.umap"));
	FileManager.FindFiles(ImagesFound, *FullPath, TEXT("*.uasset"));

	for (SIZE_T i = 0; i < ImagesFound.Num(); ++i)
	{
		if (ImagesFound[i].Contains("BuiltData"))
		{
			ImagesFound.RemoveAt(i);
			--i;
		}
	}

	// There should be an equal amount of elements in both arrays
	if (MapsFound.Num() == ImagesFound.Num())
	{
		for (SIZE_T i = 0; i < MapsFound.Num(); ++i)
		{
			MapsFound[i].RemoveFromEnd(".umap");
			ImagesFound[i].RemoveFromEnd(".uasset");
			UE_LOG(LogTemp, Warning, TEXT("%s, %s"), *MapsFound[i], *ImagesFound[i]);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("There is not an equal amount of maps and images in: %s... Did you remember to save everything?"), *FullPath);
		return false;
	}
	return true;
}

// Called when the game starts or when spawned
void AMapSelection::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMapSelection::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}