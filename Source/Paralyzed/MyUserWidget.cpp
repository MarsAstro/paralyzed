// Copyright OSS 2017

#include "MyUserWidget.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/CanvasPanel.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/UMG/Public/Components/ScaleBox.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"

/**
* Creates a UButton and returns it.
* @param ButtonName - The name of the button, when the player clicks this button
*  its name will be sent forward to the server, thus representing the player's choice.
*/
UButton* UMyUserWidget::CreateButton(const FString& ButtonName = "")
{

	UButton* Button;
	
	// Creates a UButton with default name.
	if (ButtonName == "")
	{
		Button = NewObject<UButton>(UButton::StaticClass());
	}
	else
	{
		Button = NewObject<UButton>(UButton::StaticClass(), *ButtonName);
	}

	Button->SetVisibility(ESlateVisibility::Visible);
	return Button;
}

UCanvasPanel* UMyUserWidget::CreateCanvasPanel()
{
	UCanvasPanel* CanvasPanel = NewObject<UCanvasPanel>(UCanvasPanel::StaticClass());
	CanvasPanel->SetVisibility(ESlateVisibility::Visible);
	return CanvasPanel;
}

/**
* Creates a UImage with optional opportunity to choose a texture right away.
* @param Path - The path to the asset (often starts with Game/).
* @param TextureName - The name of the asset itself.
*/
UImage* UMyUserWidget::CreateImage(const FString& Path = "", const FString& TextureName = "")
{
	UImage* Image = NewObject<UImage>(UImage::StaticClass());
	Image->SetVisibility(ESlateVisibility::Visible);

	if (Path != "" && TextureName != "")
	{
		FString AssetPath = "Texture2D'/{Path}/{TextureName}.{TextureName}'";
		AssetPath = AssetPath.Replace(TEXT("{TextureName}"), *TextureName).Replace(TEXT("{Path}"), *Path);
		FStringAssetReference AssetRef(AssetPath);
	
		Image->SetBrushFromTexture(Cast<UTexture2D>(AssetRef.TryLoad()), false);
	}

	return Image;
}

UScaleBox* UMyUserWidget::CreateScaleBox()
{
	UScaleBox* ScaleBox = NewObject<UScaleBox>(UImage::StaticClass());
	ScaleBox->SetVisibility(ESlateVisibility::Visible);
	return ScaleBox;
}

/**
* Creates a UTextBlock object (Text in widget) and assigns optional Text.
* @param Text - Text to give the text block.
*/
UTextBlock* UMyUserWidget::CreateTextBlock(const FString& Text = "")
{
	UTextBlock* TextBlock = NewObject<UTextBlock>(UImage::StaticClass());
	TextBlock->SetVisibility(ESlateVisibility::Visible);
	TextBlock->SetText(FText::FromString(Text));
	return TextBlock;
}

UTexture2D* UMyUserWidget::GetTextureByName(const FString& Path, const FString& TextureName)
{

	FString AssetPath = "Texture2D'/{Path}/{TextureName}.{TextureName}'";
	AssetPath = AssetPath.Replace(TEXT("{TextureName}"), *TextureName).Replace(TEXT("{Path}"), *Path);
	FStringAssetReference AssetRef(AssetPath);

	return Cast<UTexture2D>(AssetRef.TryLoad());
}
