// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RaceManager.generated.h"

class UCheckpoint;
class AWheelchairPawn;

UCLASS()
class PARALYZED_API ARaceManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARaceManager();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void UpdatePlacements();

	int GetNumberOfLaps() { return NumberOfLaps; };

	UFUNCTION(BlueprintCallable, Category = "Finished Race")
	void RacerFinished(AWheelchairPawn* Racer);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	TArray<AWheelchairPawn*> GetRacers() { return Racers; };

	UFUNCTION(BlueprintCallable, Category = "Setup")
	TArray<AWheelchairPawn*> GetFinalPlacements() { return FinalPlacements; };

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetupRacers(UCheckpoint* Start, TArray<FVector> StartPositions);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
	int NumberOfLaps;

private:
	// The players in the race. 
	// Can be both AI and human controlled players.
	TArray<AWheelchairPawn*> Racers;
	TArray<AWheelchairPawn*> FinalPlacements;
	

	int MaxPlayers;
	float RaceTimer;
	bool bRaceStarted;
};
