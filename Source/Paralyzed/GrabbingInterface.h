// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "GrabbingInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGrabbingInterface : public UInterface
{
	GENERATED_BODY()
};

class PARALYZED_API IGrabbingInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Grabbing")
	void Grabbed(UMotionControllerComponent* GrabbingController);
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Grabbing")
	void Released(UMotionControllerComponent* GrabbingController);
};
