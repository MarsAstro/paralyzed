// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TextureHandler.generated.h"

class UTexture2D;

/**
 * 
 */
UCLASS()
class PARALYZED_API UTextureHandler : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = TextureHandler)
		static UTexture2D* GetTextureByName(const FString& Path, const FString& TextureName);
};
