// Copyright OSS 2017

#include "Killzone.h"
#include "Components/BoxComponent.h"
#include "Checkpoint.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "WheelchairPawn.h"

// Sets default values
AKillzone::AKillzone()
{
}

void AKillzone::Initialize(UBoxComponent* TriggerVolume)
{
	this->TriggerVolume = TriggerVolume;
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AKillzone::OnOverlapBegin);
	
}

void AKillzone::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
							   AActor* OtherActor,
							   UPrimitiveComponent* OtherComp,
							   int32 OtherBodyIndex,
							   bool bFromSweep,
							   const FHitResult &SweepResult)
{
	AWheelchairPawn* Racer = Cast<AWheelchairPawn>(OtherActor);

	if (Racer == nullptr)
	{
		return;
	}

	UCheckpoint* Checkpoint = Racer->LastVisitedCheckpoint;
	Racer->PlaceAt(Checkpoint->GetComponentTransform().GetLocation() + FVector(0.f, 0.f, 100.f), 
				(FVector::CrossProduct(Checkpoint->GetRightVector(), -Checkpoint->GetUpVector())).ToOrientationQuat());
}

// Called when the game starts or when spawned
void AKillzone::BeginPlay()
{
	Super::BeginPlay();
}


