// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "GrabbableObject.h"
#include "Flashlight.generated.h"

class USpotLightComponent;

/**
 * 
 */
UCLASS()
class PARALYZED_API AFlashlight : public AGrabbableObject
{
	GENERATED_BODY()

public:
	AFlashlight();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Togglees light if held by a controller
	void ToggleLight();

private:
	/*
	* Calls original grabbed function and saves if grabbed or not
	*/
	void Grabbed_Implementation(UMotionControllerComponent* GrabbingController) override;
	void Released_Implementation(UMotionControllerComponent* GrabbingController) override;

	UStaticMeshComponent* LightBulb = nullptr;
	USpotLightComponent* SpotLight = nullptr;
	bool bIsGrabbed = false;
};
