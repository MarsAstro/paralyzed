// Copyright OSS 2017

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GrabbingInterface.h"
#include "GrabbableObject.generated.h"

class UMotionControllerComponent;

/**
 * Generic class for objects in the world that can be picked up by the player.
 * To discern between the type of objects picked up, use the Unreal Engine tagging system.
 */
UCLASS()
class PARALYZED_API AGrabbableObject : public AStaticMeshActor, public IGrabbingInterface
{
	GENERATED_BODY()
public:
	AGrabbableObject();

protected:
	void Grabbed_Implementation(UMotionControllerComponent* GrabbingController) override;
	void Released_Implementation(UMotionControllerComponent* GrabbingController) override;

private:
	UMotionControllerComponent* OwningController = nullptr;
};
