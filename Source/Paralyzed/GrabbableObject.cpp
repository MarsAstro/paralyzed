// Copyright OSS 2017

#include "GrabbableObject.h"
#include "GrabMotionController.h"
#include "MotionControllerComponent.h"
#include "Components/StaticMeshComponent.h"

AGrabbableObject::AGrabbableObject()
{
	// No need for this class to tick, as it is event-driven
	PrimaryActorTick.bCanEverTick = false;

	// Sets up physics details
	GetStaticMeshComponent()->SetSimulatePhysics(true);
	GetStaticMeshComponent()->SetCollisionProfileName("BlockAllDynamic");
	GetStaticMeshComponent()->bGenerateOverlapEvents = true;
	GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
	GetStaticMeshComponent()->WakeRigidBody();
}

// Shouldn't simulate physics when grabbed, as it could lead to grabbed objects 
// colliding with the wheelchair

void AGrabbableObject::Grabbed_Implementation(UMotionControllerComponent* GrabbingController)
{
	// If a hand grabs the object while it is already grabbed by another hand
	// we don't want the old hand to still be able to release the object
	if (OwningController != nullptr && OwningController != GrabbingController)
	{
		Cast<AGrabMotionController>(OwningController->GetOwner())->RelinquishObjectOwnership();
		GetRootComponent()->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld, true));
	}
	
	OwningController = GrabbingController;

	// Shouldn't simulate physics when grabbed, as it could lead to grabbed objects 
	// colliding with the wheelchair
	GetStaticMeshComponent()->SetSimulatePhysics(false);
	GetStaticMeshComponent()->SetCollisionProfileName("OverlapAllDynamic");
	GetRootComponent()->AttachToComponent(GrabbingController, FAttachmentTransformRules(EAttachmentRule::KeepWorld, false));
}


void AGrabbableObject::Released_Implementation(UMotionControllerComponent* GrabbingController)
{
	OwningController = nullptr;

	// Physics return to normal when object is released
	GetStaticMeshComponent()->SetSimulatePhysics(true);
	GetStaticMeshComponent()->SetCollisionProfileName("BlockAllDynamic");
	GetRootComponent()->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld, true));
}