// Copyright OSS 2017

#include "GrabMotionController.h"
#include "Components/SphereComponent.h"
#include "GrabbingInterface.h"
#include "MotionControllerComponent.h"
#include "Components/SkeletalMeshComponent.h"

// Sets default values
AGrabMotionController::AGrabMotionController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGrabMotionController::BeginPlay()
{
	Super::BeginPlay();
}

void AGrabMotionController::Initialize(UMotionControllerComponent* MotionController, USkeletalMeshComponent* HandMesh, USphereComponent* GrabSphere)
{
	this->MotionController = MotionController;
	this->HandMesh = HandMesh;
	this->GrabSphere = GrabSphere;

	// Makes sure the left hand isn't just a second right hand
	if (this->MotionController->Hand == EControllerHand::Left)
	{
		this->GrabSphere->ComponentTags.Add("LeftController");
		this->HandMesh->SetWorldScale3D(FVector(1, 1, -1));
	}
	else
	{
		this->GrabSphere->ComponentTags.Add("RightController");
	}
}

void AGrabMotionController::GrabFirstGrabbableObject()
{
	// Initialize may not have been called
	if (!GrabSphere || !MotionController)
	{
		return;
	}

	// Impossible to know if one or more candidates are overlapping, so all have to be checked
	TArray<AActor*> OverlappedActors;
	GrabSphere->GetOverlappingActors(OverlappedActors);

	for (AActor* const Actor : OverlappedActors)
	{
		// No need to handle anything other than the first object that implements the interface
		if (Actor->GetClass()->ImplementsInterface(UGrabbingInterface::StaticClass()))
		{
			IGrabbingInterface* GrabbingInterface = Cast<IGrabbingInterface>(Actor);
			GrabbingInterface->Execute_Grabbed(Actor, MotionController);

			// Keeping a reference to the successfully grabbed object means the release function
			// won't have to loop through any arrays
			if (GrabbedObject == nullptr)
			{
				GrabbedObject = Actor;
			}

			break;
		}
	}
}

void AGrabMotionController::ReleaseGrabbedObject()
{	
	// Releasing while not holding an object should do nothing
	if (GrabbedObject != nullptr)
	{
		IGrabbingInterface* GrabbingInterface = Cast<IGrabbingInterface>(GrabbedObject);
		GrabbingInterface->Execute_Released(GrabbedObject, MotionController);

		// No need to release the same object twice
		GrabbedObject = nullptr;
	}
}

void AGrabMotionController::RelinquishObjectOwnership()
{
	GrabbedObject = nullptr;
}

void AGrabMotionController::RumbleController(float Intensity)
{
	if (!HasRumbled)
	{
		Rumble.Broadcast(Intensity);
		HasRumbled = true;
	}
}

void AGrabMotionController::ResetRumble()
{
	HasRumbled = false;
}

AActor* AGrabMotionController::GetGrabbedObjectReference()
{
	return GrabbedObject;
}

void AGrabMotionController::SetGrabbedObjectReference(AActor* Item)
{
	GrabbedObject = Item;
}

UMotionControllerComponent* AGrabMotionController::GetMotionControllerComponent()
{
	return MotionController;
}
