// Copyright OSS 2017

#include "Flashlight.h"

#include "Engine/StaticMesh.h"
#include "Components/SpotLightComponent.h"
#include "ConstructorHelpers.h"
#include "Classes/Materials/Material.h"

AFlashlight::AFlashlight() : AGrabbableObject()
{
	SpotLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("SpotLight"));
	GetStaticMeshComponent()->SetRelativeScale3D(FVector(3,3,3));

	LightBulb = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LightBulb"));
	LightBulb->SetRelativeScale3D(FVector(0.07f, 0.07f, 0.07f));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	if (SphereMeshAsset.Succeeded())
	{
		LightBulb->SetStaticMesh(SphereMeshAsset.Object);
	}
	LightBulb->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	
	static ConstructorHelpers::FObjectFinder<UMaterial> BulbMaterial(TEXT("Material'/Game/Props/Div/M_LightBulb.M_LightBulb'"));
	if (BulbMaterial.Succeeded())
	{
		LightBulb->SetMaterial(0, BulbMaterial.Object);
	}
}

void AFlashlight::BeginPlay()
{
	SpotLight->AttachToComponent(GetStaticMeshComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "Light location");
	LightBulb->AttachToComponent(GetStaticMeshComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "Light location");

	// Turn Light since mesh forward is 90 degrees off
	SpotLight->AddLocalRotation(FRotator(0, -90, 0));

	UWorld* World = GetWorld();
	if (World)
	{
		World->GetFirstPlayerController()->InputComponent->BindAction("ToggleFlashlight", IE_Pressed, this, &AFlashlight::ToggleLight);
	}
}

void AFlashlight::ToggleLight()
{
	if (bIsGrabbed)
	{
		SpotLight->SetVisibility(!SpotLight->IsVisible());
	}
}

void AFlashlight::Grabbed_Implementation(UMotionControllerComponent * GrabbingController)
{
	AGrabbableObject::Grabbed_Implementation(GrabbingController);
	bIsGrabbed = true;
}

void AFlashlight::Released_Implementation(UMotionControllerComponent * GrabbingController)
{
	AGrabbableObject::Released_Implementation(GrabbingController);
	bIsGrabbed = false;
}
